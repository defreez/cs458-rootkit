typedef enum _SYSTEM_INFORMATION_CLASS {
    SystemBasicInformation = 0,
    SystemPerformanceInformation = 2,
    SystemTimeOfDayInformation = 3,
    SystemProcessInformation = 5,
    SystemProcessorPerformanceInformation = 8,
    SystemInterruptInformation = 23,
    SystemExceptionInformation = 33,
    SystemRegistryQuotaInformation = 37,
    SystemLookasideInformation = 45
} SYSTEM_INFORMATION_CLASS;

NTSTATUS
HookZwQuerySystemInformation(
	IN SYSTEM_INFORMATION_CLASS sysInfoClass,
	IN OUT PVOID sysInfo,
	IN ULONG sysInfoLen,
	OUT PULONG RetLen OPTIONAL);

NTSYSAPI
NTSTATUS
NTAPI
ZwQuerySystemInformation(
    IN ULONG SystemInformationClass,
    IN OUT PVOID SystemInformation,
    IN ULONG SystemInformationLength,
    OUT PULONG ReturnLength);

// Function pointer to ZwQuerySystemInformation prior to hook
typedef NTSTATUS (*ZWQUERYSYSTEMINFORMATION)(ULONG, PVOID, ULONG, PULONG);
ZWQUERYSYSTEMINFORMATION OldZwQuerySystemInformation;

// KeServiceDescriptorTable is of this type
typedef struct ServiceDescriptorEntry {
    unsigned int *ServiceTableBase;
    unsigned int *ServiceCounterTableBase; 
    unsigned int NumberOfServices;
    unsigned char *ParamTableBase;
} ServiceDescriptorTableEntry, *PServiceDescriptorTableEntry;

typedef struct _SYSTEM_PROCESSES   
{ // System Information Class 5  
     ULONG           NextEntryDelta;   
     ULONG           ThreadCount;  
     ULONG           Reserved1[6];  
     LARGE_INTEGER   CreateTime;  
     LARGE_INTEGER   UserTime;  
     LARGE_INTEGER   KernelTime;  
     UNICODE_STRING  ProcessName;  
     ULONG           BasePriority;  
     ULONG           ProcessId;  
     ULONG           InheritedFromProcessId;  
     ULONG           HandleCount;  
     ULONG           Reserved2[2];  
} SYSTEM_PROCESSES, * PSYSTEM_PROCESSES; 
